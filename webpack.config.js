var debug = false;
var webpack = require('webpack');
const path = require('path');

module.exports = {
  context: __dirname,
  devtool: false,
  entry: './javascript/client.js',
  output: {
    path: __dirname + "/public",
    filename: "client.min.js"
  },
  plugins: debug ? [] : [],
};
