/*
Gateway - client-side script

This is used to generate the page header, for templates,
printing dynamic content, universal error notifications
across platforms, device registrations, and verifying
browser compatibility with the user's device.

Author: Ty Everett
License: MIT
*/

// dependencies
var bch = require('bitcoincashjs');
import IO from 'socket.io-client';
var $ = require('jquery');

// verify sessionStorage is securely implemented
// [TODO] Google SessionStorage so all browsers are compatible in future
if(typeof(sessionStorage) == 'undefined'){
	displayError("SessionStorage is not supported by your browser. Please use a modern browser such as Google Chrome or the mobile app.", 1);
}

// Global variables:
//  serverConnection: WebSocket connecting to the Gateway server
var serverConnection;


// universal error function. Prints errors in a platform-agnostic
// and standard way.
// Arguments:
//  message: a specific description of the error or situation
//  isFatal: a boolean variable indicating if the code should crash and stop
function displayError(message, isFatal = 0){
	// compose the final message the user will see
	// this is based on if the error is fatal or not.
	// additionally, fatal errors could use different styling
	if(isFatal){
		message = "A fatal error has caused a crash: " + message;
	}else{
		message = "An error has occurred: " + message;
	}
	
	// check for Android and display with Android.displayError()
	
	// check for iOS and display with iOS.displayError();
	
	// if the error cannot be displayed via the methods above
	// then bootstrap is used
	
	// if this fails or is not yet implemented
	// then the standard alert() function is used
	alert(message);
	
	// if the error was fatal, stop execution of all javascript
	if(isFatal){
		throw new Error(message);
	}
}

// function to register device IDs with the server.
// For use with the mobile apps, HTML5 notifications,
// and other ways of associating devices with users.
function registerDevice(){
	// verify that the user is logged in (using sessionStorage)
	
	// verify user is on a mobile device using the app
	//  [TODO] OR, that HTML5 notifications are supported
	
	// obtain the device ID from the Android or iOS JavaScript interface
	
	// tell the serverConnection WebSocket "register <deviceId> <username> <signature>"
	//  where signature is a copy of the message signed with the user's
	//  private key
	
	// obtain the response, displaying an error if applicable
	
}

// function for signing a message with the user's Bitcoin private key
// Arguments:
//  message: the message to be signed
// Returns:
//  a string containing the signed message in base64
function signMessageWithPrivateKey(message){
	// verify user is logged in
	
	// sign the message
	
	// check for error
	
	// return signed message
	
}


// Generates an address given a seed
function getAddress(seed){
	
}

// returns the balance in satoshis of an address
function getBalance(address){
	
}

// sends "amount" satoshis from privkey "priv" to destination "destinationAddress"
function sendAmount(amount, privkey, destinationAddress){
	
	return 1;
}


// function for checking if user is logged in
function isLoggedIn(){
	if(sessionStorage.isLoggedIn == 1){
		return true;
	}else{
		return false;
	}
}

// function for logging into the service
// Arguments:
//  username: the username, email, phone number, or bitcoin address of the user
//  password: the password used for authentication
function logIn(username, password){
	// verify we are not logged in already
	if(isLoggedIn()){
		logOut();
	}
	// take the hash of the password to send the server,
	//  NOTING THAT WE NEVER SHARE THE ACTUAL PASSWORD WITH THE SERVER, EVER.
	passwordHash = sha512(password);
	
	// send command to server
	/*serverConnection.emit({
		command: 'login',
		username: username,
		password: passwordHash
	};
	serverConnection.on('loginResult')*/
	
}

// function to log out
function logOut(){
	// verify we are logged in
	if(isLoggedIn()){
		// tell serverConnection "logout"
		
		// update SessionStorage to reflect the change
		sessionStorage.isLoggedIn = 0;
	}else{
		displayError("You are not logged in and have tried to log out.", 0);
	}
}

// this should be executed on page load.
$(document).ready(function(){
	// check to see if this is the first time this has been run this session
	if(typeof sessionStorage.isNotFirstLoad == "undefined"){ // first load
		// code to run on initial setup of the session should go here.
		
		// register the device with the service
		registerDevice();
		
		
		
		// set firstLoad flag to be correct
		sessionStorage.isNotFirstLoad = 1;
	}
	
	// code run EVERY TIME a page loads should be placed here
	
	// connect to the WebSocket and store in a global variable
	//  [!] SECURITY HOLE POTENTIAL [!]
	serverConnection = IO.connect('ws://' + window.location.host);
	
	// verify it works, ping-pong, if not then show an error
	
	// handle socket receive events on serverConnection
	
});
