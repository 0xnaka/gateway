<h1>Gateway</h1>
<h2>An Innovative Bitcoin Cash Platform</h2>
<p>Gateway is a universal cross-site authentication system allowing bitcoin
cash transactions in a peer-to-peer manner. Users only need one account, and
they can log in across multiple websites to send payments.</p>
<h2>HELP WANTED</h2>
<p>We're always looking for new developers to contribute code, ideas, graphics
and suggestions. Get in touch with us on Discord!</p>
<a href="https://discord.gg/kHQcrGe" target="_blank">https://discord.gg/kHQcrGe</a>
