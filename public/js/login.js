/*
Gateway.cash - client-side login script

Author: Ty Everett
License: MIT

*/

// define variables for the user's public key, private key, and 
var a, k, p;

$(document).ready(function(){
	$('#loginform').on('submit', function(e){
		e.preventDefault();
		user = $('#username').val();
		pass = $('#password').val();
		hex = sha512(user + " bitcoin cash " + pass);
		hex = hex.slice(7, 31);
		m = Mnemonic.fromHex(hex);
    words = m.toWords();
		pseed = sha512(words);
		initialize(pseed);
	});
	$('#forgotform').on('submit', function(e){
		e.preventDefault();
		words = $('#recovery').val();
		// TODO: stop the users from being fucktards
		// what I mean is, make the recovery phrase standard (one space between words,
		// all caps, no commas, etc, betfore hashing it).
		// but also try hashing it different ways if that doesn't work in case ther're
		// just tards and managed to get it wrong in the beginning
		pseed = sha512(words);
		initialize(pseed);
	});
});

// initializes wallet when user logs in with seed
function initialize(seed){
	// set up session storage
	
	// 2xTODO: check for credentials stored on device
	
	// generate addresses based on credentials
	if(!getAddress(seed)){
		alert("Unable to generate Bitcoin addresses and keys for your credentials. Try again?");
		// how do we die() in JavaScript?
		return -1;
	}
	// DEBUG:
	alert('login successful!\nAddress: ' + a + '\nBalance: TODO'); 
	// add any mobile device UIDs to firebase
	
	// register any addresses as listening for notificationss with server
	
	// save vars to local storage
	
	// redirect to main page
}

// generates an address given a seed
function getAddress(seed){
	d = BigInteger.fromByteArraySigned(Crypto.SHA256(seed));
	k = new Bitcoin.ECKey(d);
	a = k.getBitcoinAddress();
	p = k.getBitcoinWalletImportFormat();
	return 1; // todo error checking
}

// returns the balance in satoshis of an address
function getBalance(address){
	
	return 0;
}

// sends "a" satoshis from privkey "p" to destination "d"
function sendAmount(p, a, d){
	
	return 1;
}